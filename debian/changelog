matita (0.99.1-3) unstable; urgency=medium

  * Fix FTBFS w.r.t. lablgtk2 2.16 by updating findlinb name for gtksourceview
    (Closes: #731702) 
  * New patch to help the type checker in nCicUntrusted.set_kind

 -- Enrico Tassi <gareuselesinge@debian.org>  Tue, 10 Dec 2013 20:15:50 +0100

matita (0.99.1-2) unstable; urgency=low

  * Fix clean target (Closes: 724163) 

 -- Enrico Tassi <gareuselesinge@debian.org>  Wed, 02 Oct 2013 11:16:08 +0200

matita (0.99.1-1) unstable; urgency=low

  * New upstream release
  * Removed package matita-doc (doc is available via F1)
  * Switch to dh
  * Add matita.desktop
  * Remove the following patches (integrated upstream):
    - Fix-FTBFS-with-camlp5-6.05
    - matita.conf.xml.in
    - slist-sep
    - numbers
    - native-compilers
  * New patch 'configure' to avoid useless build-dep on mysql ocaml bindings
  * Cleanup debian 

 -- Enrico Tassi <gareuselesinge@debian.org>  Sun, 06 May 2012 20:35:57 +0200

matita (0.5.8-4) unstable; urgency=medium

  * Team upload
  * Fix FTBFS with camlp5 6.05 (Closes: #666594)

 -- Stéphane Glondu <glondu@debian.org>  Sun, 01 Apr 2012 12:15:20 +0200

matita (0.5.8-3) unstable; urgency=low

  [ Stéphane Glondu ]
  * Team upload
  * Switch packaging to git
  * Switch source package format to 3.0 (quilt)
  * Remove Stefano from Uploaders

  [ Colin Watson ]
  * Apply r11210 from upstream SVN to account for changes in the
    signatures of Gramext.Slist0sep and Gramext.Slist1sep (Closes: #612891)

 -- Stéphane Glondu <glondu@debian.org>  Tue, 31 May 2011 11:17:47 +0200

matita (0.5.8-2) unstable; urgency=low

  * Conflict and replace matita-standard-library (Closes: #559261)
  * suggest matita-doc

 -- Enrico Tassi <gareuselesinge@debian.org>  Fri, 04 Dec 2009 14:32:45 +0100

matita (0.5.8-1) unstable; urgency=low

  * New upstream release supporting lablgtksourceview2
  * Added docbook-xsl and docbook-xml as build-depend-indep (Closes: #532289)
  * Removed debian/patches/lablgtk2.14.dpatch, added upstream 
  * Added README.source
  * build-depend on lablgtksourceview2
  * build-depend on debhelper >= 5
  * Standards-version set to 3.8.3 

 -- Enrico Tassi <gareuselesinge@debian.org>  Wed, 02 Dec 2009 00:10:57 +0100

matita (0.5.7-2) unstable; urgency=low

  * rebuild against ocaml 3.11
  * copyright file points to versioned licenses

 -- Enrico Tassi <gareuselesinge@debian.org>  Fri, 20 Mar 2009 18:19:50 +0100

matita (0.5.7-1) unstable; urgency=low

  * New upstream release

 -- Enrico Tassi <gareuselesinge@debian.org>  Sun, 15 Feb 2009 16:04:54 +0100

matita (0.5.6-1) experimental; urgency=low

  * New upstream bugfix release

 -- Enrico Tassi <gareuselesinge@debian.org>  Mon, 01 Dec 2008 19:11:51 +0100

matita (0.5.5-2) experimental; urgency=low

  * Bumped standards-version to 3.8.0, no changes needed
  * Added matita-doc package to contain the .html and .pdf versions of 
    the user manual
  * Added matita-doc.doc-base to register the user manual shipped by 
    matita-doc
  * Relaxed dependency of matita-standard-library to >= making it 
    bin-nmuable. The bin-nmu is safe only if the OCaml compiler used to
    build the binary package has the same marshalling convention
    of the one used to build the arch-all package. (Closes: #454176)

 -- Enrico Tassi <gareuselesinge@debian.org>  Sat, 22 Nov 2008 15:45:34 +0100

matita (0.5.5-1) experimental; urgency=low

  * New upstream bugfix release

 -- Enrico Tassi <gareuselesinge@debian.org>  Mon, 17 Nov 2008 18:24:52 +0100

matita (0.5.4-1) experimental; urgency=low

  * New upstream bugfix release

 -- Enrico Tassi <gareuselesinge@debian.org>  Sun, 19 Oct 2008 10:24:36 +0200

matita (0.5.3-1) unstable; urgency=low

  * New upstream bugfix release

 -- Enrico Tassi <gareuselesinge@debian.org>  Wed, 23 Jul 2008 23:32:19 +0200

matita (0.5.2-1) unstable; urgency=low

  * New upstream release

 -- Enrico Tassi <gareuselesinge@debian.org>  Wed, 02 Jul 2008 11:39:06 +0200

matita (0.5.1-1) unstable; urgency=medium

  * New upstream release

 -- Enrico Tassi <gareuselesinge@debian.org>  Thu, 29 May 2008 10:14:28 +0200

matita (0.5.0-1) unstable; urgency=low

  * New upstream release

 -- Enrico Tassi <gareuselesinge@debian.org>  Fri, 09 May 2008 18:47:36 +0200

matita (0.5.0~rc1-1) unstable; urgency=low

  * New upstream release candidate, fixing many relevant bugs and cleaning up
    the standard library of theorems
  * Strict dependency over libgtkmathview >= 0.8.0-2

 -- Enrico Tassi <gareuselesinge@debian.org>  Thu, 01 May 2008 14:11:10 +0200

matita (0.4.98-7) unstable; urgency=medium

  * Added dependency on the bytecode interpreted (ocaml-base-nox-$OCAMLABI)
    on architectures that do not compile in native code.

 -- Enrico Tassi <gareuselesinge@debian.org>  Mon, 03 Mar 2008 10:34:52 +0100

matita (0.4.98-6) unstable; urgency=medium

  [ Stefano Zacchiroli ]
  * setting me and Enrico as uploaders, d-o-m as maintainer
  * fixing vcs-svn field with the new, official field name
  * update standards-version, no changes needed

  [ Enrico Tassi ]
  * rebuilt against ocaml 3.10.1 

 -- Enrico Tassi <gareuselesinge@debian.org>  Sun, 02 Mar 2008 22:27:52 +0100

matita (0.4.98-5) unstable; urgency=low

  * Added patch to disallow native compilers on alpha and ia64

 -- Enrico Tassi <gareuselesinge@debian.org>  Sat, 08 Dec 2007 11:29:31 +0100

matita (0.4.98-4) unstable; urgency=low

  * Bumped version on camlp5 >= 5.04 and ulex08 >= 0.8-4
  * Added patch to remove dependency on Coq numbers in number_notation.ml

 -- Enrico Tassi <gareuselesinge@debian.org>  Wed, 05 Dec 2007 13:45:27 +0100

matita (0.4.98-3) unstable; urgency=low

  * Bumped again dependencies over liblablgtkmathview-ocaml-dev 

 -- Enrico Tassi <gareuselesinge@debian.org>  Tue, 27 Nov 2007 10:31:55 +0100

matita (0.4.98-2) unstable; urgency=low

  * updated dependency among liblablgtkmathview-ocaml-dev to
    possible fix FTBFS 

 -- Enrico Tassi <gareuselesinge@debian.org>  Mon, 26 Nov 2007 19:14:53 +0100

matita (0.4.98-1) unstable; urgency=low

  [ Stefano Zacchiroli ]
  * fix Vcs-* fields to match pkg-ocaml-maint repository settings

  [ Enrico Tassi ]
  * Fixed dependencies among camlp5, ulex08 and findlib (Closes: #451896)

 -- Enrico Tassi <gareuselesinge@debian.org>  Tue, 20 Nov 2007 13:47:26 +0100

matita (0.4.97-1) unstable; urgency=low

  * New version svn tag 0.4.96.

 -- Enrico Tassi <gareuselesinge@debian.org>  Fri, 16 Nov 2007 20:37:55 +0100

matita (0.4.96-1) unstable; urgency=low

  * First upload of svn tag 0.4.96 (Closes: #448156).

 -- Enrico Tassi <gareuselesinge@debian.org>  Fri, 26 Oct 2007 11:25:03 +0200

